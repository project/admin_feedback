## Admin feedback

### Setup / Installation
1. Download and install the module as usual.
2. Add the feedback block (Admin Feedback Block) to the block layout.
3. Navigate to /admin/feedback to see the dashboard.

### Any issues?
Please feel free to use the [Drupal.org Issue Queue for the module](https://www.drupal.org/project/issues/admin_feedback).
