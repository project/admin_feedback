<?php

/**
 * @file
 * Code for the Admin Feedback module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function admin_feedback_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.admin_feedback':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t("Advanced Content Feedback (initially named admin_feedback) makes it very easy to gather user feedback about the content of your site.") . '</p>';
      $output .= '<p>' . t("Both global and per content dashboards exist to make it easy for you to analyze the quality of your content as it perceived by your visitors.") . '</p>';

      return ['#markup' => $output];
  }
}

/**
 * Feedback theme function.
 */
function admin_feedback_theme($existing, $type, $theme, $path) {

  return [
    'views_view_table__feedback__nodes_list' => [
      'render element' => 'elements',
      'template' => 'views/views-view-table--feedback--nodes-list',
      'base hook' => 'views_view_table',
    ],

    'views_view_table__feedback__nodes_score' => [
      'render element' => 'elements',
      'template' => 'views/views-view-table--feedback--nodes-score',
      'base hook' => 'views_view_table',
    ],

    'views_view_field__feedback__nodes_score__inspected' => [
      'render_element' => 'elements',
      'template' => 'views/views-view-field--feedback--nodes-score--inspected',
      'base hook' => 'views_view_field',
    ],

    'feedback_mail_template' => [
      'render element' => 'elements',
      'template' => 'feedback_mail_template',
      'variables' => ['feedback_data' => NULL],
    ],
  ];
}

/**
 * Implements hook_views_pre_view().
 */
function admin_feedback_views_pre_view(ViewExecutable $view, $display_id, array &$args) {
  if ($view->id() === 'feedback') {
    // Needed for invalidating the cache for our custom feedback db table.
    $view->storage->addCacheTags(['feedback_cache_tags']);
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function admin_feedback_preprocess_views_view_table(&$variables) {
  if (isset($variables['view']->current_display)
    && $variables['view']->current_display === 'nodes_score'
  ) {
    $feedback_node_id = $variables['view']->args[0];

    $languages = \Drupal::languageManager()->getLanguages();

    foreach ($languages as $langcode => $language) {
      $langname = $language->getName();
      $query = \Drupal::database()->select('admin_feedback_score', 't');
      $query->fields('t', ['yes_count', 'no_count', 'count', 'total_score']);
      $query->condition("nid", $feedback_node_id);
      $query->condition("langcode", $langcode);

      $result = $query->execute()->fetchAssoc() ?:
        array_fill_keys(['yes_count', 'no_count', 'count', 'total_score'], 0);

      $variables['node_feedback_total'][$langcode]['language'] = $langname;
      $variables['node_feedback_total'][$langcode]['total_count'] = $result['count'];
      $variables['node_feedback_total'][$langcode]['yes_count'] = $result['yes_count'];
      $variables['node_feedback_total'][$langcode]['no_count'] = $result['no_count'];
      $variables['node_feedback_total'][$langcode]['total_score'] = $result['total_score'];
    }
  }
}

/**
 * Implements hook_views_query_alter().
 */
function admin_feedback_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  if ($view->id() === 'feedback' && $view->current_display === 'nodes_list') {
    $table = $query->getTableInfo('node_field_data_admin_feedback_score');
    $table['join']->extra[] = [
      'field' => 'langcode',
      'left_table' => 'admin_feedback_score',
      'left_field' => 'langcode',
    ];
  }
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function admin_feedback_node_delete(EntityInterface $entity) {
  $tables = ['admin_feedback', 'admin_feedback_score'];

  foreach ($tables as $table) {
    \Drupal::database()->delete($table)
      ->condition('nid', $entity->id())
      ->execute();
  }
}

/**
 * Implements hook_ENTITY_TYPE_translation_delete().
 */
function admin_feedback_node_translation_delete(EntityInterface $translation) {
  $tables = ['admin_feedback', 'admin_feedback_score'];

  foreach ($tables as $table) {
    \Drupal::database()->delete($table)
      ->condition('nid', $translation->id())
      ->condition('langcode', $translation->language()->getId())
      ->execute();
  }
}
