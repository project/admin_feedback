/**
 * @File
 * Javascript for the Admin Feedback module.
 */

(function ($, Drupal, drupalSettings) {
  $(document).ready(function () {
    $(".feedback-btn").click(function (event) {
      var yesResponse = drupalSettings.admin_feedback.yes_response;
      var noResponse = drupalSettings.admin_feedback.no_response;
      var feedbackPromptOnYes = drupalSettings.admin_feedback.feedback_prompt_on_yes;
      var feedbackPromptOnNo = drupalSettings.admin_feedback.feedback_prompt_on_no;
      var customTextResponseOnNo = (drupalSettings.admin_feedback.custom_text_response_on_no) ? drupalSettings.admin_feedback.custom_text_response_on_no.value : '';
      var feedbackPrompt = drupalSettings.admin_feedback.feedback_prompt;
      var feedbackEnablePredefinedAnswers = drupalSettings.admin_feedback.feedback_enable_predefined_answers;
      var nodeId = $(this).data('node-id');
      var feedback = $(this).data('feedback');
      var langCode = drupalSettings.path.currentLanguage;
      var cancelTimeoutEnabled = drupalSettings.admin_feedback.feedback_allow_cancel.active;
      var cancelTimeoutDuration = drupalSettings.admin_feedback.feedback_allow_cancel.timeout * 1000;

      const createFeedbackForm = function () {
        if (customTextResponseOnNo === null) {
          customTextResponseOnNo = '<p></p>';
        }

        $("#edit-feedback-send").prop("disabled", true);
        if (feedbackEnablePredefinedAnswers === true) {
          $("#feedback-msg-answers").click(function() {
            if ($("#feedback-msg-answers input").is(":checked")) {
              $("#edit-feedback-send").prop("disabled", false);
              $("#edit-feedback-send").css("cursor", "pointer");
            }
          });
        } else {
          $('#edit-feedback-message').val('');
          $('#edit-feedback-message').keyup(function () {
            if ($(this).val() != '') {
              $('#edit-feedback-send').prop('disabled', false);
              $('#edit-feedback-send').css('cursor', 'pointer');
            } else if ($(this).val() == '') {
              $('#edit-feedback-send').prop('disabled', true);
              $('#edit-feedback-send').css('cursor', 'not-allowed');
            }
          });
        }

        if (feedback == 'yes') {
          if (feedbackPromptOnYes === true) {
            $('#upper-feedback-content').replaceWith('<h2>' + yesResponse + '</h2>');
            if (feedbackEnablePredefinedAnswers === true) {
              $('#feedback-msg-answers legend').text(feedbackPrompt);
            } else {
              $('#edit-feedback-label').text(feedbackPrompt);
            }
            $('#admin-feedback-form').css('display', 'block');
          } else {
            $('#upper-feedback-content').replaceWith('<h2>' + yesResponse + '</h2>');
          }
        } else if (feedback == 'no') {
          if (feedbackPromptOnNo === true) {
            $('#upper-feedback-content').replaceWith('<h2>' + noResponse + '</h2>');
            if (feedbackEnablePredefinedAnswers === true) {
              $('#feedback-msg-answers legend').text(feedbackPrompt);
            } else {
              $('#edit-feedback-label').text(feedbackPrompt);
            }
            $('#admin-feedback-form').css('display', 'block');
          } else {
            $('#upper-feedback-content').replaceWith('<h2>' + noResponse + '</h2>' + customTextResponseOnNo);
          }
        }

        var langPrefix = drupalSettings.path.pathPrefix === drupalSettings.path.currentLanguage + '/'
          ? '/' + drupalSettings.path.currentLanguage
          : '';

        $.ajax({
          type: "POST",
          url: langPrefix + "/feedback_vote",
          data: {"vote": feedback, "node_id": nodeId},
          success: function (data) {
            $('#feedback-id').val(data[0]);
          },
        });
      }

      if (cancelTimeoutEnabled) {
        $('.feedback-btn__yes').hide();
        $('.feedback-btn__no').hide();

        const cancelButton = $(`<button id='feedback-cancel' class='button feedback-btn feedback-btn__cancel'>${Drupal.t('Cancel')}</button>`);

        const cancelTimeoutCounter = $(`
        <div class="progress admin_feedback__progress_bar" data-drupal-progress>
          <div class="progress__track"><div class="progress__bar" style="animation-duration: ${cancelTimeoutDuration}ms"></div></div>
        </div>
        `);

        $("#upper-feedback-content").append(cancelButton).append(cancelTimeoutCounter);

        const timeout = setTimeout(createFeedbackForm, cancelTimeoutDuration);

        cancelButton.click(function () {
          clearTimeout(timeout);

          cancelButton.remove();
          cancelTimeoutCounter.remove();

          $('.feedback-btn__yes').show();
          $('.feedback-btn__no').show();
        });

        return;
      }

      createFeedbackForm();
    });
  });
})(jQuery, Drupal, drupalSettings);
