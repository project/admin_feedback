<?php

namespace Drupal\admin_feedback\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Database\Connection;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Drupal\admin_feedback\Event\VoteEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Controller for the admin_feedback module.
 */
class AdminFeedbackController extends ControllerBase {

  /**
   * Feedback 'yes' value.
   *
   * @var int
   */
  const ADMIN_FEEDBACK_YES_VALUE = 1;

  /**
   * Feedback 'no' value.
   *
   * @var int
   */
  const ADMIN_FEEDBACK_NO_VALUE = 0;

  /**
   * The Database Connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The RequestStack object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The current language key.
   *
   * @var string
   */
  protected $langcode;

  /**
   * Event Dispatcher Service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('request_stack'),
      $container->get('language_manager'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * AdminFeedbackController constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   */
  final public function __construct(Connection $database, RequestStack $requestStack, LanguageManagerInterface $languageManager, EventDispatcherInterface $eventDispatcher) {
    $this->database = $database;
    $this->requestStack = $requestStack->getCurrentRequest();
    $this->langcode = $languageManager->getCurrentLanguage()->getId();
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * Function for getting single nodes score.
   *
   * @param int $node_id
   *   The node id.
   */
  public function getCurrentNodeScore($node_id = NULL) {
    if ($node_id != NULL) {
      $connection = $this->database->select('admin_feedback_score', 'f')
        ->fields('f', ['id', 'count', 'yes_count', 'no_count', 'total_score'])
        ->condition('nid', $node_id)
        ->condition('langcode', $this->langcode)
        ->execute()
        ->fetchAll();
      return $connection;
    }
  }

  /**
   * Function for inserting rows to database.
   *
   * @param string $feedback
   *   The feedback.
   * @param int $node
   *   The node id.
   */
  public function insertFeedback($feedback = NULL, $node = NULL) {
    $query_method = 'insert';
    $record = [
      'nid' => $node,
      'langcode' => $this->langcode,
      'created' => time(),
      'feedback_type' => $feedback,
      'feedback_message' => NULL,
    ];
    $query = $this->database->$query_method('admin_feedback')->fields($record);
    if ($query_method == 'upsert') {
      $query->key('id');
    }
    $last_inserted_id = $query->execute();
    $encoded64_id = base64_encode($last_inserted_id);

    // Invalidate cache for our custom db table.
    Cache::invalidateTags(['feedback_cache_tags']);

    return $encoded64_id;
  }

  /**
   * Function for updating rows in database.
   *
   * @param int $feedback_id
   *   The feedback id.
   * @param string $feedback_message
   *   The feedback message.
   */
  public function updateFeedback($feedback_id = NULL, $feedback_message = NULL) {
    if (!empty($feedback_id)) {
      $feedback_id = base64_decode($feedback_id);
    }

    if ($feedback_message != NULL && !empty($feedback_message)) {
      $this->database->update('admin_feedback')
        ->fields([
          'feedback_message' => $feedback_message,
        ])
        ->condition('id', $feedback_id)
        ->execute();
    }
  }

  /**
   * Function for inserting score.
   *
   * @param int $node_id
   *   The node id.
   * @param int $feedback
   *   The feedback count.
   * @param int $count
   *   Total count.
   */
  public function insertScore($node_id, $feedback, $count) {
    $yes = 0;
    $no = 0;
    $count = $count + 1;
    ($feedback == 1) ? $yes = 1 : $no = 1;
    ($feedback == 1) ? $total_score = 100 : $total_score = 0;
    $query_method = 'insert';
    $record = [
      'nid' => $node_id,
      'langcode' => $this->langcode,
      'count' => $count,
      'yes_count' => $yes,
      'no_count' => $no,
      'total_score' => $total_score,
    ];
    $query = $this->database->$query_method('admin_feedback_score')->fields($record);
    $query->execute();
  }

  /**
   * Function for updating score.
   *
   * @param int $node_id
   *   The node id.
   * @param int $feedback
   *   The feedback count.
   * @param int $count
   *   Total count.
   * @param int $total_score
   *   The total score.
   * @param int $yes_count
   *   Yes count.
   * @param int $no_count
   *   No count.
   */
  public function updateScore($node_id, $feedback, $count, $total_score, $yes_count, $no_count) {
    if ($node_id != NULL && !empty($node_id)) {
      ($feedback == 1) ? $yes_count = $yes_count + 1 : $no_count = $no_count + 1;
      $count = $count + 1;
      $total_score = round($yes_count / $count * 100);
      $this->database->update('admin_feedback_score')
        ->fields([
          'count' => $count,
          'yes_count' => $yes_count,
          'no_count' => $no_count,
          'total_score' => $total_score,
        ])
        ->condition('nid', $node_id)
        ->condition('langcode', $this->langcode)
        ->execute();
    }
  }

  /**
   * Function for receiving votes.
   */
  public function adminFeedbackVoteReceiver() {
    $feedback = $this->requestStack->request->get('vote');
    $node_id = $this->requestStack->request->get('node_id');

    // Dispatch a Vote event, in case any module needs
    // to update this with custom logic.
    $event = new VoteEvent();
    $event->setNid($node_id);
    $event->setVote($feedback);
    $this->eventDispatcher->dispatch($event, VoteEvent::VOTE_EVENT);

    switch ($feedback) {
      case 'yes':
        $feedback = self::ADMIN_FEEDBACK_YES_VALUE;
        break;

      case 'no':
        $feedback = self::ADMIN_FEEDBACK_NO_VALUE;
        break;

      default:
        $feedback = NULL;
        break;
    }
    $last_inserted_vote_id[] = $this->insertFeedback($feedback, $node_id);

    $node_score = $this->getCurrentNodeScore($node_id);
    if (!$node_score) {
      $count = 0;
      $this->insertScore($node_id, $feedback, $count);
    }
    else {
      $count = $node_score[0]->count;
      $total_score = $node_score[0]->total_score;
      $yes_count = $node_score[0]->yes_count;
      $no_count = $node_score[0]->no_count;
      $this->updateScore($node_id, $feedback, $count, $total_score, $yes_count, $no_count);
    }

    return new JsonResponse($last_inserted_vote_id, 200, ['Content-Type' => 'application/json']);
  }

  /**
   * Function for marking inspections.
   */
  public function markInspected() {
    $response = new AjaxResponse();
    $feedback_id = $this->requestStack->request->get('feedback_id');

    if ($feedback_id != NULL && !empty($feedback_id)) {
      $this->database->update('admin_feedback')
        ->fields([
          'inspected' => 1,
        ])
        ->condition('id', $feedback_id)
        ->execute();
    }

    // Invalidate cache.
    Cache::invalidateTags(['feedback_cache_tags']);

    return $response;
  }

  /**
   * Function for unmarking inspections.
   */
  public function markUnInspected() {
    $response = new AjaxResponse();
    $feedback_id = $this->requestStack->request->get('feedback_id');

    if ($feedback_id != NULL && !empty($feedback_id)) {
      $this->database->update('admin_feedback')
        ->fields([
          'inspected' => 0,
        ])
        ->condition('id', $feedback_id)
        ->execute();
    }

    // Invalidate cache.
    Cache::invalidateTags(['feedback_cache_tags']);

    return $response;
  }

  /**
   * Function for exporting data from the database.
   */
  public function exportDbFeedback() {
    $response = new Response();

    $query = $this->database->select('admin_feedback', 'f');
    $query->innerJoin('node_field_data', 'd', 'd.nid = f.nid');
    $query->fields(
      'f',
      [
        'id',
        'nid',
        'created',
        'feedback_type',
        'feedback_message',
        'inspected',
      ]
    );
    $query = $query->execute();
    $results = $query->fetchAll(\PDO::FETCH_OBJ);

    foreach ($results as $result) {
      if ($result->feedback_type == 1) {
        $result->feedback_type = 'positive';
      }
      elseif ($result->feedback_type == 0) {
        $result->feedback_type = 'negative';
      }
      if ($result->inspected == 1) {
        $result->inspected = 'Yes';
      }
      $result->created = date('d-m-Y', $result->created);
    }

    $output = "\xEF\xBB\xBF";
    $output .= 'Nr, URL, Created, Feedback, Message, Inspected' . "\n";

    foreach ($results as $row) {
      $url = Url::fromRoute('entity.node.canonical', ['node' => $row->nid], ['absolute' => TRUE])->toString();
      $message_formatted = '';
      if ($row->feedback_message !== NULL) {
        $message_formatted = trim(preg_replace('/\s+/', '  ', $row->feedback_message));
        $message_formatted = str_ireplace('"', '', $message_formatted);
        $message_formatted = str_ireplace("'", '', $message_formatted);
      }

      $output .= '"' . $row->id . '",';
      $output .= '"' . $url . '",';
      $output .= '"' . $row->created . '",';
      $output .= '"' . $row->feedback_type . '",';
      $output .= '"' . $message_formatted . '",';
      $output .= '"' . $row->inspected . '"';
      $output .= "\n";
    }

    $response->setContent($output);
    $response->headers->set("Content-Type", "text/csv; charset=UTF-8");
    $response->headers->set("Content-Disposition", "attachment; filename=feedback_data.csv");

    return $response;
  }

}
