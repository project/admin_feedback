<?php

namespace Drupal\admin_feedback\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Wraps a vote event for event listeners.
 */
class VoteEvent extends Event {

  /**
   * The vote variable.
   *
   * @var string
   */
  protected $vote;

  /**
   * The node id.
   *
   * @var int
   */
  protected $nid;

  const VOTE_EVENT = 'event_subscriber.vote';

  /**
   * {@inheritdoc}
   */
  public function getVote() {
    return $this->vote;
  }

  /**
   * Call to setVote method.
   *
   * @param string $vote
   *   Vote object.
   */
  public function setVote(string $vote) {
    $this->vote = $vote;
  }

  /**
   * {@inheritdoc}
   */
  public function getNid() {
    return $this->nid;
  }

  /**
   * Setting the node id.
   *
   * @param int $nid
   *   The node id.
   */
  public function setNid(int $nid) {
    $this->nid = $nid;
  }

}
