<?php

namespace Drupal\admin_feedback\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\admin_feedback\Controller\AdminFeedbackController;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The AdminFeedbackAjaxForm form class.
 */
class AdminFeedbackAjaxForm extends FormBase {

  /**
   * The Database Connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The RequestStack object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The LanguageManager object.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * Event Dispatcher Service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('request_stack'),
      $container->get('language_manager'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Construct an AdminFeedbackController object.
   */
  final public function __construct(Connection $database, RequestStack $requestStack, LanguageManagerInterface $languageManager, EventDispatcherInterface $eventDispatcher) {
    $this->database = $database;
    $this->requestStack = $requestStack;
    $this->languageManager = $languageManager;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_feedback_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory()->get('admin_feedback.settings');

    if ($config->get('feedback_enable_predefined_answers')) {
      $predefinedAnswers = $config->get('feedback_predefined_answers');
      $answers = [];
      foreach ($predefinedAnswers as $answer) {
        $answers[$answer] = $answer;
      }
      $form['feedback_message'] = [
        '#type' => 'radios',
        '#prefix' => '<div id="feedback-msg-result"></div><div id="feedback-msg-answers">',
        '#suffix' => '</div>',
        '#title' => $this->t('Select one'),
        '#options' => $answers,
      ];
    }
    else {
      $form['feedback_message'] = [
        '#type' => 'textarea',
        '#prefix' => '<div id="feedback-msg-result"></div>',
        '#attributes' => [
          'id' => 'edit-feedback-message',
        ],
        '#title' => $this->t('Extra feedback'),
        '#label_attributes' => [
          'id' => 'edit-feedback-label',
        ],
      ];
    }

    $form['feedback_id'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'feedback-id',
      ],
    ];

    $form['feedback_send'] = [
      '#type' => 'button',
      '#value' => $config->get('submit_text'),
      '#ajax' => [
        'callback' => '::validateFeedbackMsg',
      ],
      '#attributes' => [
        'class' => [
          'feedback-btn feedback-btn__send-comment',
        ],
      ],
    ];

    return $form;
  }

  /**
   * Validates the feedback form.
   */
  public function validateFeedbackMsg(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory()->get('admin_feedback.settings');

    $warning = $this->t('Form is empty!');
    $thank_you_msg = $config->get('final_response');

    $response = new AjaxResponse();

    $feedback_id = $form_state->getValue('feedback_id');
    $feedback_message = $form_state->getValue('feedback_message');

    if ($feedback_id == '' || $feedback_message == '') {
      $response->addCommand(new HtmlCommand('#feedback-msg-result', '<p class="feedback-warning">' . $warning . '</p>'));
    }
    else {
      $feedback_controller_obj = new AdminFeedbackController($this->database, $this->requestStack, $this->languageManager, $this->eventDispatcher);
      $feedback_controller_obj->updateFeedback($feedback_id, $feedback_message);
    }
    $response->addCommand(new ReplaceCommand('#feedback-message', '<h2>' . $thank_you_msg . '</h2>'));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
