<?php

namespace Drupal\admin_feedback\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Database\Database;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form for deleting a feedback response.
 *
 * @internal
 */
class AdminFeedbackDeleteForm extends ConfirmFormBase {

  /**
   * The ID of the feedback to be deleted.
   *
   * @var int
   */
  protected $feedbackId;

    /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new VariantPluginFormBase.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  final public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete feedback #%fid?', ['%fid' => $this->feedbackId]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('<front>');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_feedback_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL) {
    $this->feedbackId = $id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $connection = Database::getConnection();

    // Get info from feedback record (nid, yes/no and langcode).
    $query = $connection->select('admin_feedback', 'admin_feedback')
      ->fields('admin_feedback', ['nid', 'feedback_type', 'langcode'])
      ->condition('admin_feedback.id', $this->feedbackId, '=');
    $results = $query->execute();
    $nid = NULL;
    $node = NULL;
    $langcode = NULL;
    $feedback_type = NULL;
    foreach ($results as $result) {
      $nid = $result->nid;
      $feedback_type = $result->feedback_type;
      $langcode = $result->langcode;
    }

    // Get and update feedback score details for node and language.
    if (isset($nid) && isset($feedback_type)) {
      $node = $this->entityTypeManager->getStorage('node')->load($nid);
      $query = $connection->select('admin_feedback_score', 'admin_feedback_score')
        ->fields('admin_feedback_score',
          [
            'id',
            'count',
            'yes_count',
            'no_count',
            'total_score',
          ]
        )
        ->condition('admin_feedback_score.nid', $nid, '=')
        ->condition('admin_feedback_score.langcode', $langcode, '=');
      $results = $query->execute();
      foreach ($results as $result) {
        $score_id = $result->id;
        // Calculate new total.
        $count = $result->count - 1;

        // Calculate new score, if total votes for language are more than 0.
        if ($count > 0) {
          if ($feedback_type == 0) {
            $yes_count = $result->yes_count;
            $no_count = $result->no_count - 1;
          }
          else {
            $yes_count = $result->yes_count - 1;
            $no_count = $result->no_count;
          }

          $score = round($yes_count / $count * 100);

          // Update feedback score record for node.
          $connection->update('admin_feedback_score')
            ->fields([
              'count' => $count,
              'yes_count' => $yes_count,
              'no_count' => $no_count,
              'total_score' => $score,
            ])
            ->condition('id', $score_id, '=')
            ->execute();
        }

        // When deleting the last record delete also delete the score for this
        // node in the admin_feedback_score table. Take language into account.
        if ($count == 0) {
          $query = $connection->delete('admin_feedback_score')
            ->condition('nid', $nid, "=")
            ->condition('langcode', $langcode, "=")
            ->execute();
        }
      }
    }

    // Delete feedback record.
    $query = $connection->delete('admin_feedback')
      ->condition('id', $this->feedbackId, "=")
      ->execute();
    Cache::invalidateTags(['feedback_cache_tags']);

    // Confirm and redirect to node (or home page if no node).
    $this->messenger()->addStatus($this->t('Feedback #%fid has been deleted.', ['%fid' => $this->feedbackId]));
    if ($node) {
      $form_state->setRedirectUrl($node->toUrl());
    }
    else {
      $form_state->setRedirectUrl(Url::fromRoute('<front>'));
    }
  }

}
