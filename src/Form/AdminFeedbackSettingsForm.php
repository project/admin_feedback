<?php

namespace Drupal\admin_feedback\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * AdminFeedback Settings Form.
 */
class AdminFeedbackSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'admin_feedback.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_feedback_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('admin_feedback.settings');

    $form['#tree'] = TRUE;

    $form['initial_question'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Initial question'),
      '#description' => $this->t('The question that initially appears above the yes and no buttons.'),
      '#default_value' => $config->get('initial_question'),
      '#required' => TRUE,
    ];
    $form['yes_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Yes button text'),
      '#description' => $this->t('Text that appears on the Yes button in the Feedback Block'),
      '#default_value' => $config->get('yes_button'),
      '#required' => TRUE,
    ];
    $form['yes_response'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Yes response'),
      '#description' => $this->t('Response that appears when the user selects Yes.'),
      '#default_value' => $config->get('yes_response'),
      '#required' => TRUE,
    ];
    $form['no_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('No button text'),
      '#description' => $this->t('Text that appears on the No button in the Feedback Block'),
      '#default_value' => $config->get('no_button'),
      '#required' => TRUE,
    ];
    $form['no_response'] = [
      '#type' => 'textfield',
      '#title' => $this->t('No response'),
      '#description' => $this->t('Response that appears when the user selects No.'),
      '#default_value' => $config->get('no_response'),
      '#required' => TRUE,
    ];

    // Extra Feedback after Yes/No.
    $form['feedback_prompt_on_yes'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ask for extra feedback on "Yes"'),
      '#description' => $this->t('Display an extra <strong>form</strong> (free text or predefined) to the user upon clicking the "Yes" button.'),
      '#default_value' => $config->get('feedback_prompt_on_yes'),
      '#required' => FALSE,
    ];
    $form['feedback_prompt_on_no'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ask for extra feedback on "No"'),
      '#description' => $this->t('Display an extra <strong>form</strong> (free text or predefined) to the user upon clicking the "No" button.'),
      '#default_value' => $config->get('feedback_prompt_on_no'),
      '#required' => FALSE,
    ];

    // Custom text on negative feedback.
    $form['custom_text_response_on_no'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Custom text to be displayed on "No" answer if no feedback is asked'),
      '#description' => $this->t('Response that appears when the user has
        clicked "No" and we don\'t ask for more feedback.<br>
         It will <strong>only</strong> be used if "Ask for extra feedback on No" above is <strong>unchecked</strong>.<br>
        Usefull to add custom functionallity/interaction to this module like:<br><ul>
        <li>Add a custom link towards your issue queue system.</li>
        <li>Display a button to send an email.</li></ul>'),
      '#default_value' => $config->get('custom_text_response_on_no.value'),
      '#format' => $config->get('custom_text_response_on_no.format'),
      '#required' => FALSE,
      '#states' => [
        // Show only if feedback_prompt_on_no above is unchecked.
        'visible' => [
          ':input[name="feedback_prompt_on_no"]' => ['checked' => FALSE],
        ],
      ],
    ];

    // Comment Answer.
    $form['comment'] = [
      '#type' => 'details',
      '#title' => $this->t('Feedback Prompt Settings'),
      '#open' => TRUE,
      '#weight' => 10,
      '#description' => $this->t('Presents a form to the user to submit an extra comment.'),
      '#states' => [
        'visible' => [
          [':input[name="feedback_prompt_on_yes"]' => ['checked' => TRUE]],
          'or',
          [':input[name="feedback_prompt_on_no"]' => ['checked' => TRUE]],
        ],
      ],
    ];
    $form['comment']['feedback_prompt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Feedback prompt'),
      '#description' => $this->t('Text that requests additional feedback after the user has selected an option.'),
      '#default_value' => $config->get('feedback_prompt'),
      '#required' => TRUE,
    ];
    $form['comment']['submit_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Submit button text'),
      '#description' => $this->t('Text in the button the user uses to submit their additional feedback.'),
      '#default_value' => $config->get('submit_text'),
      '#required' => TRUE,
    ];
    $form['comment']['final_response'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Final response'),
      '#description' => $this->t('Response that appears when the user has submitted additional feedback.'),
      '#default_value' => $config->get('final_response'),
      '#required' => TRUE,
    ];

    // Predefined Answers.
    $form['comment']['feedback_enable_predefined_answers'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display a list of predefined answers <strong>instead of</strong> the comment form.'),
      '#description' => $this->t('Uses a list of predefinded answers like
        "Content Outdated", "Translation is bad", "Not Helpful", etc.<br>
        You most probably want to disable the extra feedback on positive answers
        buy unchecking <em>Ask for extra feedback on "Yes"</em> above.'),
      '#default_value' => $config->get('feedback_enable_predefined_answers'),
      '#required' => FALSE,
    ];

    $form['comment']['predefined'] = [
      '#type' => 'details',
      '#title' => $this->t('Predefined Feedback Answers'),
      '#open' => TRUE,
      '#weight' => 20,
      '#description' => $this->t('Uses predefined answers instead of comment form.'),
      '#states' => [
        'visible' => [
          ':input[name="comment[feedback_enable_predefined_answers]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['comment']['predefined']['answers_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Predefined answers'),
      '#prefix' => '<div id="answers-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];

    // Get already saved answers.
    $predefined_answers_list = $config->get('feedback_predefined_answers');
    // We have to ensure that there is at least one answer field.
    $feedback_predefined_answers = $form_state->get('feedback_predefined_answers');
    if ($feedback_predefined_answers === NULL) {
      if (count($predefined_answers_list) == 0) {
        $form_state->set('feedback_predefined_answers', 1);
        $feedback_predefined_answers = 1;
      }
      else {
        $form_state->set('feedback_predefined_answers', count($predefined_answers_list));
        $feedback_predefined_answers = count($predefined_answers_list);
      }
    }

    for ($i = 0; $i < $feedback_predefined_answers; $i++) {
      $form['comment']['predefined']['answers_fieldset']['answer'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('Answer'),
        '#default_value' => $predefined_answers_list[$i],
      ];
    }

    $form['comment']['predefined']['answers_fieldset']['actions'] = [
      '#type' => 'actions',
    ];
    $form['comment']['predefined']['answers_fieldset']['actions']['add_answer'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'answers-fieldset-wrapper',
      ],
    ];
    // If there is more than one answer, add the remove button.
    if ($feedback_predefined_answers > 1) {
      $form['comment']['predefined']['answers_fieldset']['actions']['remove_answer'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'answers-fieldset-wrapper',
        ],
      ];
    }

    $form['feedback_allow_cancel']['active'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable vote cancellation'),
      '#description' => $this->t('Allow users to cancel their vote during submission.'),
      '#default_value' => $config->get('feedback_allow_cancel.active'),
      '#format' => $config->get('feedback_allow_cancel.active.format'),
    ];

    $form['feedback_allow_cancel']['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Set cancellation interval'),
      '#description' => $this->t('Set the time interval, in seconds, for a user to cancel their submission. Decimal values are allowed.'),
      '#default_value' => $config->get('feedback_allow_cancel.timeout'),
      '#min' => .5,
      '#step' => .1,
      '#states' => [
        // Show only if feedback_prompt_on_no above is unchecked.
        'visible' => [
          ':input[name="feedback_allow_cancel[active]"]' => ['checked' => TRUE],
        ],
        'required' => [
          ':input[name="feedback_allow_cancel[active]"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the answers in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['comment']['predefined']['answers_fieldset'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $answer_field = $form_state->get('feedback_predefined_answers');
    $add_button = $answer_field + 1;
    $form_state->set('feedback_predefined_answers', $add_button);
    // Since our buildForm() method relies on the value of
    // 'feedback_predefined_answers' to generate 'answer' form elements, we have
    // to tell the form to rebuild. If we don't do this, the form builder will
    // not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $answer_field = $form_state->get('feedback_predefined_answers');
    if ($answer_field > 1) {
      $remove_button = $answer_field - 1;
      $form_state->set('feedback_predefined_answers', $remove_button);
    }
    // Since our buildForm() method relies on the value of
    // 'feedback_predefined_answers' to generate 'answer' form elements, we have
    // to tell the form to rebuild. If we don't do this, the form builder will
    // not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('admin_feedback.settings')
      ->set('initial_question', $form_state->getValue('initial_question'))
      ->set('yes_button', $form_state->getValue('yes_button'))
      ->set('yes_response', $form_state->getValue('yes_response'))
      ->set('no_button', $form_state->getValue('no_button'))
      ->set('no_response', $form_state->getValue('no_response'))
      ->set('feedback_prompt_on_yes', $form_state->getValue('feedback_prompt_on_yes'))
      ->set('feedback_prompt_on_no', $form_state->getValue('feedback_prompt_on_no'))
      ->set('feedback_prompt', $form_state->getValue(
          [
            'comment',
            'feedback_prompt',
          ]
        )
      )
      ->set('submit_text', $form_state->getValue(['comment', 'submit_text']))
      ->set('final_response', $form_state->getValue(
          [
            'comment',
            'final_response',
          ]
        )
      )
      ->set('custom_text_response_on_no', $form_state->getValue('custom_text_response_on_no'))
      ->set('feedback_enable_predefined_answers', $form_state->getValue(
          [
            'comment',
            'feedback_enable_predefined_answers',
          ]
        )
      )
      // Only save valid answers, discard empty ones.
      ->set('feedback_predefined_answers',
        array_values(array_filter(
        $form_state->getValue(['comment', 'predefined', 'answers_fieldset', 'answer']))))
      ->set('feedback_allow_cancel', $form_state->getValue('feedback_allow_cancel'))
      ->save();
  }

}
