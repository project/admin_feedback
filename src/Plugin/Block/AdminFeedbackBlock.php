<?php

namespace Drupal\admin_feedback\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBuilder;
use Drupal\Core\Render\Markup;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the Admin Feedback Block.
 *
 * @Block(
 *   id = "admin_feedback_block",
 *   admin_label = @Translation("Admin Feedback Block"),
 * )
 */
class AdminFeedbackBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The CurrentRouteMatch object.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  private $routeMatch;

  /**
   * The FormBuilder object.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  private $builder;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('current_route_match'),
      $container->get('form_builder')
    );
  }

  /**
   * AdminFeedbackBlock constructor.
   */
  final public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory, CurrentRouteMatch $routeMatch, FormBuilder $builder) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $configFactory;
    $this->routeMatch = $routeMatch;
    $this->builder = $builder;
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'give feedback');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->configFactory->get('admin_feedback.settings');

    $build = [];
    $node = $this->routeMatch->getParameter('node');

    if ($node && !is_string($node)) {

      $nid = $node->id();

      $build = [
        'feedback_content' => [
          '#type' => 'container',
          '#attributes' => [
            'id' => ['feedback-message'],
          ],
          'upper_text' => [
            '#type' => 'container',
            '#attributes' => [
              'id' => ['upper-feedback-content'],
            ],
            'form_title' => [
              '#type' => 'html_tag',
              '#tag' => 'h2',
              '#value' => $config->get('initial_question'),
            ],
            'yes_button' => [
              '#type' => 'button',
              '#value' => $config->get('yes_button'),
              '#prefix' => '<span class="feedback-yes-button">',
              '#suffix' => '</span>',
              '#markup' => Markup::create('<label class="visually-hidden" for="feedback-yes-button-' . $nid . '">' . $config->get('yes_button') . '</label>'),
              '#attributes' => [
                'class' => ['feedback-btn feedback-btn__yes'],
                'id' => ['feedback-yes-button-' . $nid],
                'data-node-id' => $node->id(),
                'data-feedback' => 'yes',
              ],
            ],
            'no_button' => [
              '#type' => 'button',
              '#value' => $config->get('no_button'),
              '#prefix' => '<span class="feedback-no-button">',
              '#suffix' => '</span>',
              '#markup' => Markup::create('<label class="visually-hidden" for="feedback-no-button-' . $nid . '">' . $config->get('no_button') . '</label>'),
              '#attributes' => [
                'class' => ['feedback-btn feedback-btn__no'],
                'id' => ['feedback-no-button-' . $nid],
                'data-node-id' => $node->id(),
                'data-feedback' => 'no',
              ],
            ],
          ],
          'form' => $this->builder
            ->getForm('Drupal\admin_feedback\Form\AdminFeedbackAjaxForm'),
        ],
        '#attached' => [
          'library' => [
            'admin_feedback/admin_feedback_block',
            'admin_feedback/admin_feedback_css',
          ],
          'drupalSettings' => [
            'admin_feedback' => [
              'yes_response' => $config->get('yes_response'),
              'no_response' => $config->get('no_response'),
              'custom_text_response_on_no' => $config->get('custom_text_response_on_no'),
              'feedback_prompt_on_yes' => $config->get('feedback_prompt_on_yes'),
              'feedback_prompt_on_no' => $config->get('feedback_prompt_on_no'),
              'feedback_enable_predefined_answers' => $config->get('feedback_enable_predefined_answers'),
              'feedback_prompt' => $config->get('feedback_prompt'),
              'feedback_allow_cancel' => $config->get('feedback_allow_cancel'),
            ],
          ],
        ],
      ];
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return Cache::mergeContexts(
      parent::getCacheContexts(),
      ['user.permissions', 'url.path']
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return Cache::mergeTags(
      parent::getCacheTags(),
      ['config:admin_feedback.settings']
    );
  }

}
